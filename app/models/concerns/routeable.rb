# Utility concern to access to the routes helper from outside the request context.
# We use ActionMailer host configuration, as in config/environment.
#
# Whenever this behavior is changed, this file should be updated accordingly.
# https://www.dwightwatson.com/posts/accessing-rails-routes-helpers-from-anywhere-in-your-app/
module Routeable
  extend ActiveSupport::Concern

  included do
    include Rails.application.routes.url_helpers
  end

  def default_url_options
    { host: Rails.application.config.site.host, port: Rails.application.config.site.port }
  end
end
