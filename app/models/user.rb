class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  delegate :follows?, to: :actor

  validates :username, presence: true, uniqueness: true, format: { with: /[a-z0-9\-_]{3,}/i }

  has_one :actor, dependent: :destroy

  scope :confirmed, -> { where.not(confirmed_at: nil) }

  after_create :create_actor

  def name
    attributes['name'] || "@#{username}"
  end

  def confirmed?
    confirmed_at.present?
  end

  private

  def create_actor
    Actor.create! user: self
  end
end
