class ActorsController < ApplicationController
  before_action :set_actor, only: [:show]

  # GET /actors
  # GET /actors.json
  def index
    @actors = policy_scope Actor.all
  end

  # GET /actors/1
  # GET /actors/1.json
  def show; end

  # GET /explorer/lookup
  # GET /explorer/lookup.json
  def lookup
    @actor = Actor.find_by_account account_param
    authorize @actor
    render :show
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_actor
    @actor = Actor.find(params[:id])
    authorize @actor
  end

  def account_param
    params.require('account')
  end
end
