module Federation
  class NotesController < FederationApplicationController
    before_action :set_note, only: [:show]

    # GET /federation/actors/1/notes/1.json
    def show; end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_note
      @note = Note.find_by!(actor_id: params[:actor_id], id: params[:id])
      authorize @note
    end
  end
end
