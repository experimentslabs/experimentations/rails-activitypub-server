class NotesController < ApplicationController
  before_action :set_note, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, except: [:index, :show]

  # GET /notes
  # GET /notes.json
  def index
    @notes = policy_scope Note.all
  end

  # GET /notes/1
  # GET /notes/1.json
  def show; end

  # GET /notes/new
  def new
    @note = Note.new
  end

  # GET /notes/1/edit
  def edit; end

  # POST /notes
  # POST /notes.json
  def create # rubocop:disable Metrics/AbcSize
    @note       = Note.new(note_params)
    @note.actor = current_user.actor

    respond_to do |format|
      if @note.save
        format.html { redirect_to @note, notice: I18n.t('controller.notes.create.success') }
        format.json { render :show, status: :created, location: @note }
      else
        format.html { render :new }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /notes/1
  # PATCH/PUT /notes/1.json
  def update
    respond_to do |format|
      if @note.update(note_params)
        format.html { redirect_to @note, notice: I18n.t('controller.notes.update.success') }
        format.json { render :show, status: :ok, location: @note }
      else
        format.html { render :edit }
        format.json { render json: @note.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /notes/1
  # DELETE /notes/1.json
  def destroy
    @note.destroy
    respond_to do |format|
      format.html { redirect_to notes_url, notice: I18n.t('controller.notes.destroy.success') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_note
    @note = Note.find(params[:id])
    authorize @note
  end

  # Only allow a list of trusted parameters through.
  def note_params
    params.require(:note).permit(:content)
  end
end
