class ActivitiesController < ApplicationController
  before_action :authenticate_user!, only: [:feed]

  # GET /activities
  # GET /activities.json
  def index
    @activities = policy_scope Activity.all
    @activities = @activities.where actor_id: params[:actor_id] if params[:actor_id]
  end

  def feed
    @activities = Activity.feed_for(current_user.actor)
  end
end
