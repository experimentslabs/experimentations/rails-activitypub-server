class FollowingsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_following, only: [:accept, :destroy]

  def accept
    respond_to do |format|
      if @following.accept!
        format.html { redirect_to current_user, notice: I18n.t('controller.followings.accept.success') }
        format.json { render :show, status: :ok, location: @following }
      else
        format.html { redirect_to current_user, alert: I18n.t('controller.followings.accept.error') }
        format.json { render json: @following.errors, status: :unprocessable_entity }
      end
    end
  end

  # POST /followings
  # POST /followings.json
  def create
    @following       = Following.new(following_params)
    @following.actor = current_user.actor
    authorize @following

    save_and_render
  end

  # POST /followings/follow
  # POST /followings/follow.json
  def follow
    begin
      @following = Following.new_from_account following_account_params, actor: current_user.actor
      authorize @following
    rescue ActiveRecord::RecordNotFound
      # Renders a 422 instead of a 404
      respond_to do |format|
        format.html { redirect_to current_user, alert: I18n.t('controller.followings.follow.error') }
        format.json { render json: { target_actor: ['does not exist'] }, status: :unprocessable_entity }
      end

      return
    end

    save_and_render
  end

  # DELETE /followings/1
  # DELETE /followings/1.json
  def destroy
    @following.destroy
    respond_to do |format|
      format.html { redirect_to current_user, notice: I18n.t('controller.followings.destroy.success') }
      format.json { head :no_content }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_following
    @following = Following.find(params[:id])
    authorize @following
  end

  # Only allow a list of trusted parameters through.
  def following_params
    params.require(:following).permit(:target_actor_id)
  end

  def following_account_params
    params.require(:account)
  end

  def save_and_render
    respond_to do |format|
      if @following.save
        format.html { redirect_to current_user, notice: I18n.t('controller.followings.save_and_render.success') }
        format.json { render :show, status: :created, location: @following }
      else
        format.html { redirect_to current_user, alert: I18n.t('controller.followings.save_and_render.error') }
        format.json { render json: @following.errors, status: :unprocessable_entity }
      end
    end
  end
end
