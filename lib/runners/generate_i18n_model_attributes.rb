# frozen_string_literal: true

def add_i18n_tasks_use(key)
  @content += "# i18n-tasks-use t('#{key}')\n"
end

def add_i18n_tasks_use_for_activerecord_model(model)
  add_i18n_tasks_use("activerecord.models.#{model}")
end

def add_i18n_tasks_use_for_activerecord_attribute(model, attribute)
  add_i18n_tasks_use("activerecord.attributes.#{model}.#{attribute}")
end

I18N_FILE = File.join('app', 'i18n', 'model_attributes.i18n')
FileUtils.rm_f(I18N_FILE)

EXCLUDED_MODELS = [
  'ApplicationRecord',
].freeze

@content = ''

Dir.glob(File.join('app', 'models', '*.rb')).sort.each do |f|
  class_name = File.basename(f, '.rb').camelize

  next if EXCLUDED_MODELS.include? class_name

  model = class_name.split('::').join('.').underscore
  Rails.logger.debug { "Checking for model #{class_name} (#{model})" }
  add_i18n_tasks_use_for_activerecord_model(model)

  # Standard attributes
  class_obj = class_name.constantize
  class_obj.attribute_types.sort.each do |attribute, _type|
    next if attribute == 'id'

    add_i18n_tasks_use_for_activerecord_attribute(model, attribute.sub(/_id$/, ''))
  end
end

comment = "# Generated using \"rake app:i18n:add-models-attributes\" - Do not modify manually\n"
File.write(I18N_FILE, "#{comment}\n#{@content}\n#{comment}\n")
