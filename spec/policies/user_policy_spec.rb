require 'rails_helper'
require 'pundit/rspec'

RSpec.describe UserPolicy, type: :policy do
  let(:signed_in_user) { FactoryBot.create :user, :confirmed }
  let(:scope) { Pundit.policy_scope!(nil, User) }
  let(:user) { FactoryBot.create :user, :confirmed }

  permissions '.scope' do
    it 'returns all the confirmed users' do
      FactoryBot.create :user
      FactoryBot.create :user, :confirmed

      # Plus the one created in the "before :suite" in rails helper
      expect(scope.all.count).to eq 2
    end
  end

  permissions :index? do
    context 'when unauthenticated' do
      it 'grants access' do
        expect(described_class).to permit(nil, User)
      end
    end

    context 'when authenticated' do
      it 'grants access' do
        expect(described_class).to permit(signed_in_user, User)
      end
    end
  end

  permissions :show? do
    context 'when unauthenticated' do
      it 'grants access to visitors' do
        expect(described_class).to permit(nil, user)
      end
    end

    context 'when authenticated' do
      it 'grants access' do
        expect(described_class).to permit(signed_in_user, user)
      end
    end
  end

  permissions :edit?, :update? do
    context 'when account is owned' do
      it 'grants access' do
        expect(described_class).to permit(signed_in_user, signed_in_user)
      end
    end

    context 'when account is not owned' do
      it 'denies access' do
        expect(described_class).not_to permit(signed_in_user, user)
      end
    end
  end
end
