require 'rails_helper'
require 'pundit/rspec'

RSpec.describe NotePolicy, type: :policy do
  let(:user) { FactoryBot.create :user, :confirmed }
  let(:owner) { FactoryBot.create :user, :confirmed }
  let(:scope) { Pundit.policy_scope!(nil, Note) }
  let(:note) { FactoryBot.create :note, actor: owner.actor }

  permissions '.scope' do
    it 'returns all the notes' do
      FactoryBot.create_list :note, 2

      expect(scope.all.count).to eq 2
    end
  end

  permissions :index? do
    context 'when unauthenticated' do
      it 'grants access' do
        expect(described_class).to permit(nil, Note)
      end
    end

    context 'when authenticated' do
      it 'grants access' do
        expect(described_class).to permit(user, Note)
      end
    end
  end

  permissions :show? do
    context 'when unauthenticated' do
      it 'grants access' do
        expect(described_class).to permit(nil, note)
      end
    end

    context 'when authenticated' do
      it 'grants access' do
        expect(described_class).to permit(user, note)
      end
    end
  end

  permissions :new?, :create? do
    context 'when unauthenticated' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, Note)
      end
    end

    context 'when authenticated' do
      it 'grants access' do
        expect(described_class).to permit(user, Note)
      end
    end
  end

  permissions :edit?, :update?, :destroy? do
    context 'when unauthenticated' do
      it 'denies access' do
        expect(described_class).not_to permit(nil, note)
      end
    end

    context 'when authenticated' do
      it 'denies access to non-owners' do
        expect(described_class).not_to permit(user, note)
      end

      it 'grants access to owner' do
        expect(described_class).to permit(owner, note)
      end
    end
  end
end
