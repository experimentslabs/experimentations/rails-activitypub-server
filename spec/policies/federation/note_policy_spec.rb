require 'rails_helper'
require 'pundit/rspec'

RSpec.describe Federation::NotePolicy, type: :policy do
  let(:user) { FactoryBot.create :user, :confirmed }
  let(:owner) { FactoryBot.create :user, :confirmed }
  let(:scope) { Pundit.policy_scope!(nil, [:federation, Note]) }
  let(:note) { FactoryBot.create :note, actor: owner.actor }

  permissions '.scope' do
    it 'returns all the notes' do
      FactoryBot.create_list :note, 2

      expect(scope.all.count).to eq 2
    end
  end

  permissions :show? do
    context 'when unauthenticated' do
      it 'grants access' do
        expect(described_class).to permit(nil, note)
      end
    end

    context 'when authenticated' do
      it 'grants access' do
        expect(described_class).to permit(user, note)
      end
    end
  end
end
