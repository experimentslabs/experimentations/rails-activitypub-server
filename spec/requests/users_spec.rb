require 'rails_helper'

RSpec.describe '/users', type: :request do
  let(:valid_attributes) { FactoryBot.build(:user).attributes }
  let(:invalid_attributes) do
    { email: '' }
  end

  let(:signed_in_user) { User.find_by email: 'user@example.com' }

  describe 'GET /index' do
    it 'renders a successful response' do
      FactoryBot.create :user
      get users_url
      expect(response).to be_successful
    end
  end

  describe 'GET /show' do
    it 'renders a successful response' do
      user = FactoryBot.create :user, :confirmed
      get user_url(user)
      expect(response).to be_successful
    end
  end

  describe 'GET /edit with unauthenticated user' do
    it 'redirects to the login page' do
      user = FactoryBot.create :user
      get edit_user_url(user)
      expect(response).to redirect_to(new_user_session_url)
    end
  end

  describe 'GET /edit with authenticated user' do
    before { sign_in signed_in_user }

    context 'when target user is not current user' do
      it 'renders an error page' do
        user = FactoryBot.create :user
        expect do
          get edit_user_url(user)
        end.to raise_error Pundit::NotAuthorizedError
      end
    end

    context 'when target user is current user' do
      it 'renders a successful response' do
        get edit_user_url(signed_in_user)
        expect(response).to be_successful
      end
    end
  end

  describe 'PATCH /update with unauthenticated user' do
    let(:new_attributes) do
      { name: 'John Doe' }
    end

    it 'redirects to the login page' do
      user = FactoryBot.create :user
      patch user_url(user), params: { user: new_attributes }
      expect(response).to redirect_to(new_user_session_url)
    end
  end

  describe 'PATCH /update with authenticated user' do
    let(:new_attributes) do
      { name: 'John Doe' }
    end

    before { sign_in signed_in_user }

    context 'when target user is not current user' do
      it 'renders an error page' do
        user = FactoryBot.create :user
        expect do
          patch user_url(user), params: { user: new_attributes }
        end.to raise_error Pundit::NotAuthorizedError
      end
    end

    context 'when target user is current user' do
      context 'with valid parameters' do
        it 'updates the requested user' do
          patch user_url(signed_in_user), params: { user: new_attributes }
          signed_in_user.reload
          expect(signed_in_user.name).to eq new_attributes[:name]
        end

        it 'redirects to the user' do
          patch user_url(signed_in_user), params: { user: new_attributes }
          signed_in_user.reload
          expect(response).to redirect_to(user_url(signed_in_user))
        end
      end

      context 'with invalid parameters' do
        it "renders a successful response (i.e. to display the 'edit' template)" do
          patch user_url(signed_in_user), params: { user: invalid_attributes }
          expect(response).to be_successful
        end
      end
    end
  end
end
