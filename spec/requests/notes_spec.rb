require 'rails_helper'

RSpec.describe '/notes', type: :request do
  let(:valid_attributes) { FactoryBot.build(:note).attributes }
  let(:invalid_attributes) do
    { content: '' }
  end

  let(:signed_in_user) { User.find_by email: 'user@example.com' }

  describe 'GET /index' do
    it 'renders a successful response' do
      FactoryBot.create :note
      get notes_url
      expect(response).to be_successful
    end
  end

  describe 'GET /show' do
    it 'renders a successful response' do
      note = FactoryBot.create :note
      get note_url(note)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    context 'with unauthenticated user' do
      it 'redirects to the login page' do
        get new_note_url
        expect(response).to redirect_to(new_user_session_url)
      end
    end

    context 'with authenticated user' do
      before { sign_in signed_in_user }

      it 'renders a successful response' do
        get new_note_url
        expect(response).to be_successful
      end
    end
  end

  describe 'GET /edit' do
    context 'with unauthenticated user' do
      it 'redirects to the login page' do
        note = FactoryBot.create :note
        get edit_note_url(note)
        expect(response).to redirect_to(new_user_session_url)
      end
    end

    context 'with authenticated user' do
      before { sign_in signed_in_user }

      it 'renders a successful response' do
        note = FactoryBot.create :note, actor: signed_in_user.actor
        get edit_note_url(note)
        expect(response).to be_successful
      end
    end
  end

  describe 'POST /create' do
    context 'with unauthenticated user' do
      it 'redirects to the login page' do
        post notes_url, params: { note: valid_attributes }
        expect(response).to redirect_to(new_user_session_url)
      end
    end

    context 'with authenticated user' do
      before { sign_in signed_in_user }

      context 'with valid parameters' do
        it 'creates a new Note' do
          expect do
            post notes_url, params: { note: valid_attributes }
          end.to change(Note, :count).by(1)
        end

        it 'redirects to the created note' do
          post notes_url, params: { note: valid_attributes }
          expect(response).to redirect_to(note_url(Note.last))
        end
      end

      context 'with invalid parameters' do
        it 'does not create a new Note' do
          expect do
            post notes_url, params: { note: invalid_attributes }
          end.not_to change(Note, :count)
        end

        it "renders a successful response (i.e. to display the 'new' template)" do
          post notes_url, params: { note: invalid_attributes }
          expect(response).to be_successful
        end
      end
    end
  end

  describe 'PATCH /update' do
    let(:new_attributes) do
      { content: 'New content' }
    end

    context 'with unauthenticated user' do
      it 'redirects to the login page' do
        note = FactoryBot.create :note
        patch note_url(note), params: { note: new_attributes }
        expect(response).to redirect_to(new_user_session_url)
      end
    end

    context 'with authenticated user' do
      before { sign_in signed_in_user }

      context 'with valid parameters' do
        it 'updates the requested note' do
          note = FactoryBot.create :note, actor: signed_in_user.actor
          patch note_url(note), params: { note: new_attributes }
          note.reload
          expect(note.content).to eq new_attributes[:content]
        end

        it 'redirects to the note' do
          note = FactoryBot.create :note, actor: signed_in_user.actor
          patch note_url(note), params: { note: new_attributes }
          note.reload
          expect(response).to redirect_to(note_url(note))
        end
      end

      context 'with invalid parameters' do
        it "renders a successful response (i.e. to display the 'edit' template)" do
          note = FactoryBot.create :note, actor: signed_in_user.actor
          patch note_url(note), params: { note: invalid_attributes }
          expect(response).to be_successful
        end
      end
    end
  end

  describe 'DELETE /destroy' do
    context 'with unauthenticated user' do
      it 'redirects to the login page' do
        note = FactoryBot.create :note
        delete note_url(note)
        expect(response).to redirect_to(new_user_session_url)
      end
    end

    context 'with authenticated user' do
      before { sign_in signed_in_user }

      it 'destroys the requested note' do
        note = FactoryBot.create :note, actor: signed_in_user.actor
        expect do
          delete note_url(note)
        end.to change(Note, :count).by(-1)
      end

      it 'redirects to the notes list' do
        note = FactoryBot.create :note, actor: signed_in_user.actor
        delete note_url(note)
        expect(response).to redirect_to(notes_url)
      end
    end
  end
end
