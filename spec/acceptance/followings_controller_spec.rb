require 'acceptance_helper'

RSpec.describe FollowingsController, type: :acceptance do
  resource 'Followings', 'Followings management'

  entity :following,
         id:              { type: :integer, description: 'Identifier' },
         actor_id:        { type: :integer, description: 'Actor creating the following' },
         target_actor_id: { type: :integer, description: 'Followed actor identifier' },
         status:          { type: :string, description: 'Request status: pending, accepted' },
         created_at:      { type: :datetime, description: 'Creation date' },
         updated_at:      { type: :datetime, description: 'Last update date' }

  entity :form_errors,
         target_actor: { type: :array, description: 'Target actor errors' }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :following_path_params,
             id: { type: :integer, description: 'Following identifier' }
  parameters :following_params_payload,
             target_actor_id: { type: :integer, description: 'Followed actor identifier' }
  parameters :account_params_payload,
             account: { type: :string, description: 'Account to check, like "ernesto" or "ayana@domain.tld"' }

  let(:user) { FactoryBot.create :user, :confirmed }
  let(:target_actor) { FactoryBot.create(:user, :confirmed, username: 'juliet').actor }
  let(:incoming_follow_request) { FactoryBot.create :following, actor: target_actor, target_actor: user.actor }

  on_post '/followings', 'Create one following request to an existing actor identifier' do
    request_params defined: :following_params_payload

    for_code 201, expect_one: :following do |url|
      sign_in user
      test_response_of url, payload: { target_actor_id: target_actor.id }
    end

    for_code 422, expect_one: :form_errors do |url|
      sign_in user
      test_response_of url, payload: { target_actor_id: -1 }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, payload: { target_actor_id: target_actor.id }
    end
  end

  on_post '/followings/follow', 'Attempts to create a following request from an account name' do
    request_params defined: :account_params_payload

    for_code 201, expect_one: :following do |url|
      target_actor
      sign_in user

      test_response_of url, payload: { account: 'juliet' }
    end

    for_code 422, expect_one: :form_errors do |url|
      sign_in user

      test_response_of url, payload: { account: 'juliet' }
    end
  end

  on_put '/followings/:id/accept', 'Accept a follow request' do
    path_params defined: :following_path_params

    for_code 200, expect_one: :following do |url|
      sign_in user

      test_response_of url, path_params: { id: incoming_follow_request.id }
    end

    for_code 401, expect_one: :error do |url|
      # Act as the user who created the follow request
      sign_in target_actor.user

      test_response_of url, path_params: { id: incoming_follow_request.id }
    end
  end

  on_delete '/followings/:id', 'Destroys one following' do
    path_params defined: :following_path_params

    for_code 204 do |url|
      following = FactoryBot.create :following, actor: user.actor, target_actor: target_actor
      sign_in user

      test_response_of url, path_params: { id: following.id }
    end

    for_code 401, expect_one: :error do |url|
      following = FactoryBot.create :following, actor: user.actor, target_actor: target_actor

      test_response_of url, path_params: { id: following.id }
    end
  end
end
