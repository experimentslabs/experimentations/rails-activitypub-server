require 'acceptance_helper'

RSpec.describe ActivitiesController, type: :acceptance do
  resource 'Activities', 'Activities management'

  entity :activity,
         id:          { type: :integer, description: 'Identifier' },
         action:      { type: :string, description: 'Performed action' },
         entity_type: { type: :string, description: 'Related object type' },
         entity_id:   { type: :integer, description: 'Related object id' },
         actor_id:    { type: :integer, description: 'Actor identifier' },
         created_at:  { type: :datetime, description: 'Creation date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  let(:user) { FactoryBot.create :user, :confirmed }
  let(:following_note) do
    followed_user = FactoryBot.create :user, :confirmed
    FactoryBot.create :following, :accepted, actor: user.actor, target_actor: followed_user.actor
    FactoryBot.create :note, actor: followed_user.actor
  end

  on_get '/', 'List activities' do
    for_code 200, expect_many: :activity do |url|
      FactoryBot.create_list :note, 2
      test_response_of url
    end
  end

  on_get '/actors/:actor_id/activities', 'List activities for a given actor' do
    path_params fields: { actor_id: { type: :integer, description: 'Actor identifier' } }

    for_code 200, expect_many: :activity do |url|
      user = FactoryBot.create :user, :confirmed
      FactoryBot.create_list :note, 2, actor: user.actor
      test_response_of url, path_params: { actor_id: user.actor.id }
    end
  end

  on_get '/feed', 'List activities for signed in user' do
    for_code 200, expect_many: :activity do |url|
      following_note
      sign_in user
      test_response_of url
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url
    end
  end
end
