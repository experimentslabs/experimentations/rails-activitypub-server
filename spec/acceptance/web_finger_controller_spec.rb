require 'acceptance_helper'

RSpec.describe WebFingerController, type: :acceptance do
  resource 'Webfinger', 'Webfinger endpoints'

  entity :webfinger,
         subject: { type: :string, description: 'Subject to find' },
         links:   { type: :array, description: 'List of available links for the actor', of: {
           rel:  { type: :string, description: 'Link descriptor' },
           type: { type: :string, description: 'Media type' },
           href: { type: :string, description: 'URL' },
         } }

  entity :node_info,
         links: { type: :array, description: '', of: {
           rel:  { type: :string, description: 'Schema URL' },
           href: { type: :string, description: 'Resource URI' },
         } }

  entity :node_info_v2,
         version:           { type: :string, description: 'NodeInfo version. Should be 2.0' },
         software:          { type: :object, description: 'Software information', attributes: {
           name:    { type: :string, description: 'Software name' },
           version: { type: :string, description: 'Version running' },
         } },
         protocols:         { type: :array, description: 'List of supported protocols' },
         services:          { type: :object, description: 'Available services', attributes: {
           inbound:  { type: :array, description: '' },
           outbound: { type: :array, description: '' },
         } },
         openRegistrations: { type: :boolean, description: 'Flag stating if registrations are open' },
         usage:             { type: :object, description: 'Usage statistics', attributes: {
           users: { type: :object, description: 'Various informations about users', attributes: {
             total:          { type: :integer, description: 'Total amount of users' },
             activeMonth:    { type: :integer, description: 'Amount of users subscribed this mounth' },
             activeHalfyear: { type: :integer, description: 'Amount of users subscribed in the last 6 months' },
           } },
         } },
         metadata:          { type: :object, description: 'Various metadata.' }

  entity :error,
         error: { type: :string, description: 'The error' }

  on_get '/.well-known/webfinger?resource=:resource', 'List activities' do
    path_params fields: { resource: { type: :string, description: 'actor address, e.g.: "acct:user@server.tld"' } }

    for_code 200, expect_one: :webfinger do |url|
      FactoryBot.create :user, username: 'roberto'
      test_response_of url, path_params: { resource: 'acct:roberto@localhost' }
    end

    for_code 404, expect_one: :error do |url|
      test_response_of url, path_params: { resource: 'acct:john@doe-service.org' }
    end
  end

  on_get '/.well-known/nodeinfo', 'List of links leading to this node infos' do
    for_code 200, expect_one: :node_info do |url|
      test_response_of url
    end
  end

  on_get '/nodeinfo/2.0', 'Node info' do
    for_code 200, expect_one: :node_info_v2 do |url|
      test_response_of url
    end
  end
end
