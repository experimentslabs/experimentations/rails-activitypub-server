require 'acceptance_helper'

RSpec.describe NotesController, type: :acceptance do
  resource 'Notes', 'Notes management'

  entity :note,
         id:         { type: :integer, description: 'Identifier' },
         content:    { type: :string, required: true, description: 'The note' },
         created_at: { type: :datetime, description: 'Creation date' },
         updated_at: { type: :datetime, description: 'Last update date' },
         actor_id:   { type: :integer, description: 'Actor identifier' }

  entity :form_errors,
         content: { type: :array, description: 'Content-related errors' }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :note_path_params,
             id: { type: :integer, description: 'Note identifier' }
  parameters :note_params_payload,
             content: { type: :string, description: 'New content' }

  let(:user) { FactoryBot.create :user, :confirmed }
  let(:note) { FactoryBot.create :note, actor: user.actor }

  on_get '/notes', 'List notes' do
    for_code 200, expect_many: :note do |url|
      FactoryBot.create_list :note, 2

      test_response_of url
    end
  end

  on_get '/notes/:id', 'Display one note' do
    path_params defined: :note_path_params

    for_code 200, expect_one: :note do |url|
      test_response_of url, path_params: { id: note.id }
    end
  end

  on_post '/notes', 'Create one note' do
    request_params defined: :note_params_payload

    for_code 201, expect_one: :note do |url|
      sign_in user

      test_response_of url, payload: { content: 'Some new note' }
    end

    for_code 422, expect_one: :form_errors do |url|
      sign_in user

      test_response_of url, payload: { content: '' }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, payload: { content: 'Some new content' }
    end
  end

  on_put '/notes/:id', 'Update one note' do
    path_params defined: :note_path_params
    request_params defined: :note_params_payload

    for_code 200, expect_one: :note do |url|
      sign_in user

      test_response_of url, path_params: { id: note.id }, payload: { content: 'Some new content' }
    end

    for_code 422, expect_one: :form_errors do |url|
      sign_in user

      test_response_of url, path_params: { id: note.id }, payload: { content: '' }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: note.id }, payload: { content: 'Some new content' }
    end
  end

  on_delete '/notes/:id', 'Destroys one note' do
    path_params defined: :note_path_params

    for_code 204 do |url|
      sign_in user

      test_response_of url, path_params: { id: note.id }
    end

    for_code 401, expect_one: :error do |url|
      test_response_of url, path_params: { id: note.id }
    end
  end
end
