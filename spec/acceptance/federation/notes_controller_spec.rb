require 'acceptance_helper'

RSpec.describe Federation::NotesController, type: :acceptance do
  resource 'Federation/Notes', 'Notes management'

  entity :note,
         '@context':   { type: :string, description: 'JSON-LD contexts' },
         id:           { type: :string, description: 'Federated id for this note' },
         type:         { type: :string, description: 'Object type. Should be "Note"' },
         url:          { type: :string, description: 'URL for human readable content' },
         attributedTo: { type: :string, description: 'Federated actor identifier' },
         content:      { type: :string, description: 'Note content' }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :note_path_params,
             actor_id: { type: :integer, description: 'Actor identifier. Not the JSON-LD identifier' },
             id:       { type: :integer, description: 'Note identifier' }

  let(:user) { FactoryBot.create :user, :confirmed }
  let(:note) { FactoryBot.create :note, actor: user.actor }

  on_get '/federation/actors/:actor_id/notes/:id', 'Display a note' do
    for_code 200, expect_one: :note do |url|
      test_response_of url, path_params: { actor_id: note.actor_id, id: note.id }
    end

    for_code 404, expect_one: :error do |url|
      test_response_of url, path_params: { actor_id: note.actor_id, id: 0 }
    end
  end
end
