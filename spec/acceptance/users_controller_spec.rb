require 'acceptance_helper'

RSpec.describe UsersController, type: :acceptance do
  resource 'Users', 'Users management'

  entity :user,
         id:           { type: :integer, description: 'Identifier' },
         username:     { type: :string, description: 'Immutable username' },
         name:         { type: :string, required: false, description: 'Display name' },
         confirmed_at: { type: :datetime, description: 'Join date' },
         updated_at:   { type: :datetime, description: 'Last update date' }

  entity :form_errors,
         email: { type: :array, description: 'Email-related errors' }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :user_path_params,
             id: { type: :integer, description: 'User identifier' }
  parameters :user_update_params,
             name:  { type: :string, description: 'Display name' },
             email: { type: :string, description: 'New email.' }

  on_get '/users', 'List users' do
    for_code 200, expect_many: :user do |url|
      FactoryBot.create_list :user, 2, :confirmed
      test_response_of url
    end
  end

  on_get '/users/:id', 'Display one user' do
    path_params defined: :user_path_params

    for_code 200, expect_one: :user do |url|
      user = FactoryBot.create :user, :confirmed
      test_response_of url, path_params: { id: user.id }
    end
  end

  on_put '/users/:id', 'Update one user. User should be logged in' do
    path_params defined: :user_path_params
    request_params defined: :user_update_params

    for_code 200, expect_one: :user do |url|
      user = FactoryBot.create :user, :confirmed
      sign_in user
      test_response_of url, path_params: { id: user.id }, payload: { email: 'new@email.com' }
    end

    for_code 422, expect_one: :form_errors do |url|
      user = FactoryBot.create :user, :confirmed
      sign_in user
      test_response_of url, path_params: { id: user.id }, payload: { email: '' }
    end

    for_code 401, expect_one: :error do |url|
      user = FactoryBot.create :user, :confirmed
      test_response_of url, path_params: { id: user.id }, payload: { email: 'new@email.com' }
    end
  end
end
