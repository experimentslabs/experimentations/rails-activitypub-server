require 'acceptance_helper'

RSpec.describe ActorsController, type: :acceptance do
  resource 'Actors', 'Actors management'

  entity :actor,
         id:             { type: :integer, description: 'Actor identifier' },
         name:           { type: :string, required: false, description: 'Name' },
         federated_url:  { type: :string, description: 'Unique identifier for federation' },
         username:       { type: :string, description: 'Immutable username' },
         inbox_url:      { type: :string, description: 'Federated inbox URL' },
         outbox_url:     { type: :string, description: 'Federated outbox URL' },
         followers_url:  { type: :string, description: 'URL to the followers list' },
         followings_url: { type: :string, description: 'URL to the followings list' },
         profile_url:    { type: :string, required: false, description: 'URL to a human readable profile' },
         at_address:     { type: :string, description: 'Federation address' },
         user_id:        { type: :integer, required: false, description: 'User identifier, for local actors' },
         created_at:     { type: :datetime, description: 'Creation date' },
         updated_at:     { type: :datetime, description: 'Last update date' }

  entity :error,
         error: { type: :string, description: 'The error' }

  parameters :actor_path_params,
             id: { type: :integer, description: 'Actor identifier' }
  parameters :account_params,
             account: { type: :string, description: 'Account to check, like "ernesto" or "ayana@domain.tld"' }

  on_get '/actors', 'List actors' do
    for_code 200, expect_many: :actor do |url|
      FactoryBot.create_list :user, 2, :confirmed
      test_response_of url
    end
  end

  on_get '/actors/:id', 'Display one actor' do
    path_params defined: :actor_path_params

    for_code 200, expect_one: :actor do |url|
      actor = FactoryBot.create(:user, :confirmed).actor
      test_response_of url, path_params: { id: actor.id }
    end

    for_code 404, expect_one: :error do |url|
      test_response_of url, path_params: { id: 0 }
    end
  end

  on_get '/actors/lookup?account=:account', 'Search for a local or distant actor' do
    path_params defined: :account_params

    for_code 200, expect_one: :actor do |url|
      FactoryBot.create :user, :confirmed, username: 'juliet'
      test_response_of url, path_params: { account: 'juliet' }
    end

    for_code 404, expect_one: :error do |url|
      test_response_of url, path_params: { account: 'juliet' }
    end
  end
end
