FactoryBot.define do
  factory :note do
    content { Faker::Lorem.paragraph }
    association :actor, factory: [:actor, :distant]
  end
end
