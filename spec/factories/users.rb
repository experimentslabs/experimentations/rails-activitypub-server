FactoryBot.define do
  factory :user do
    email { Faker::Internet.unique.email }
    password { 'password' }
    username { Faker::Internet.unique.username separators: ['-', '_'], specifier: 3 }

    trait :confirmed do
      confirmed_at { Time.current }
    end

    factory :user_known do
      email { 'user@example.com' }
    end
  end
end
