# Put your development seeds here
user = FactoryBot.create :user_known, :confirmed

FactoryBot.create_list :note, 2, actor: user.actor

# Users followed by user
FactoryBot.create_list(:user, 2, :confirmed).each do |u|
  FactoryBot.create :following, :accepted, actor: user.actor, target_actor: u.actor
end
# Users following user
FactoryBot.create_list(:user, 2, :confirmed).each do |u| # rubocop:disable Style/CombinableLoops
  FactoryBot.create :following, :accepted, actor: u.actor, target_actor: user.actor
end
# Other users
FactoryBot.create_list(:user, 2, :confirmed)
